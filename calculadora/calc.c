#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#define TEMP_SOLUTION_PATH "/tmp/rateio.temp.solution"

#define TOLERANCE 6.0
#define CASH_FLOW_WEIGHT 0.025


unsigned int nPeople;

float *globalBest;
float globalBestCost = INFINITY;



void recurse(
		float costs[nPeople][nPeople],
		short int paths[nPeople][nPeople],
		float debts[nPeople],
		float transactions[nPeople][nPeople],
		float int_cost);
void printMatrix(FILE *file, float *mat);
void simplifyCosts(float costs[nPeople][nPeople], short int paths[nPeople][nPeople]);
int isSolved(float debts[nPeople]);
float min(float v1, float v2);

int main(){
	float *costs;
	short int *paths;
	float *debts;
	float *transactions;

	scanf("%u", &nPeople);
	costs = malloc(nPeople * nPeople * sizeof(float));
	paths = malloc(nPeople * nPeople * sizeof(short int));
	debts = malloc(nPeople * nPeople * sizeof(float));
	transactions = malloc(nPeople * nPeople * sizeof(float));
	memset(transactions, 0, nPeople * nPeople * sizeof(float));
	globalBest = malloc(nPeople * nPeople * sizeof(float));

	for (int i = 0; i < nPeople; i++){
		for (int j = 0; j < nPeople; j++){
			scanf("%f", &costs[i * nPeople + j]);
			paths[i * nPeople + j] = j;
		}
	}
	for (int i = 0; i < nPeople; i++){
		scanf("%f", &debts[i]);
	}

	// Input validity check:
	double debt_sum = 0;
	for (int i = 0; i < nPeople; i++)
		debt_sum += debts[i];
	if (fabs(debt_sum) > 0.1){
		fprintf(stderr, "Input debts do not sum up to 0, aborting\n");
		return 1;
	}
	
	// Initial simplification of costs and start recursion:
	simplifyCosts((float (*)[nPeople])costs, (short int (*)[nPeople])paths);
	recurse((float (*)[nPeople])costs,
			(short int (*)[nPeople])paths,
			debts,
			(float (*)[nPeople])transactions,
			0);

	// Calculate what is left for next month and sanity check:
	for (int i = 0; i < nPeople; i++){
		double trans_sum = 0;
		for (int j = 0; j < nPeople; j++){
			trans_sum += globalBest[i * nPeople + j];
			trans_sum -= globalBest[j * nPeople + i];
		}
		printf("%.2lf ", trans_sum - debts[i]);
		if (fabs(trans_sum - debts[i]) > TOLERANCE){
			fprintf(stderr, "sanity check failed! Solution does not solve debts\n");
			return 1;
		}
	}
	printf("\n");
	
	printMatrix(stdout, globalBest);
	printf("%5.2f\n", globalBestCost);
}

void recurse(
		float costs[nPeople][nPeople],
		short int paths[nPeople][nPeople],
		float debts[nPeople],
		float transactions[nPeople][nPeople],
		float int_cost){
	// If the cost is already higher than the best solution, no point in continuing
	if (int_cost >= globalBestCost){
		return;
	}
	// Check if we are already at a solved state:
	if (isSolved(debts)){
		// Copy to best solution. We already checked if the cost is better so,
		// if we reach this point, the is definitely a better solution
		memcpy(globalBest, transactions, nPeople*nPeople*sizeof(float));
		globalBestCost = int_cost;
		// Write intermediate solutions to a file in tmpfs:
		FILE *tmp_sol = fopen(TEMP_SOLUTION_PATH, "w");
		printMatrix(tmp_sol, globalBest);
		fprintf(tmp_sol, "%5.2f\n", globalBestCost);
		fclose(tmp_sol);

		fprintf(stderr, "Better solution found with cost %f written to %s\n",
				int_cost, TEMP_SOLUTION_PATH);
		return;
	}
	// i will be someone that needs to pay(positive debt)
	for (int i = 0; i < nPeople; i++){
		// Skips if this person does not need to receive:
		if (debts[i] <= TOLERANCE / nPeople)
			continue;
		// j will be someone that needs to receive money(negative debt)
		for (int j = 0; j < nPeople; j++){
			// Skip if this person does not need to pay:
			if (debts[j] >= -TOLERANCE / nPeople)
				continue;
			// Trasnfer as much money as possible with these 2 people:
			float payment = min(debts[i], -debts[j]);
			
			// Make a copy of everything so we can create the new state and recurse
			float new_transactions[nPeople][nPeople];
			memcpy(new_transactions, transactions, nPeople * nPeople * sizeof(float));
			float new_costs[nPeople][nPeople];
			memcpy(new_costs, costs, nPeople * nPeople * sizeof(float));
			short int new_paths[nPeople][nPeople];
			memcpy(new_paths, paths, nPeople * nPeople * sizeof(short int));
			float new_debts[nPeople];
			memcpy(new_debts, debts, nPeople * sizeof(float));
			float new_cost = int_cost;
			
			// Take the shortest path to make said payment, go through all nodes:
			short int from_person = i;
			while(from_person != j){
				short int next = paths[from_person][j];
				if (transactions[from_person][next] < 0.1){
					new_cost += costs[from_person][next];
					new_costs[from_person][next] = 0;
				}
				new_transactions[from_person][next] += payment;
				new_cost += payment * CASH_FLOW_WEIGHT;
				from_person = next;
			}
			new_debts[i] -= payment;
			new_debts[j] += payment;
			simplifyCosts(new_costs, new_paths);
			recurse(new_costs, new_paths, new_debts, new_transactions, new_cost);
			
		}
	}
}

void printMatrix(FILE *file, float *mat){
	for (int i = 0; i < nPeople; i++){
		for (int j = 0; j < nPeople; j++){
			fprintf(file, "%7.2f ", mat[i * nPeople + j]);
		}
		fprintf(file, "\n");
	}
}

void simplifyCosts(float costs[nPeople][nPeople], short int paths[nPeople][nPeople]){
	// Floyd–Warshall algorithm
	for (int k = 0; k < nPeople; k++){
		for (int i = 0; i < nPeople; i++){
			for (int j = 0; j < nPeople; j++){
				if (costs[i][j] > costs[i][k] + costs[k][j]){
					costs[i][j] = costs[i][k] + costs[k][j];
					paths[i][j] = paths[i][k];
				}
			}
		}
	}
}

inline int isSolved(float debts[nPeople]){
	for (int i = 0; (i < nPeople); i++){
		if (fabs(debts[i]) > TOLERANCE)
			return 0;
	}
	return 1;
}

inline float min(float v1, float v2){
	if (v1 < v2)
		return v1;
	else
		return v2;
}

