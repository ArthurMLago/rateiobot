#!/bin/bash

for f in tests/*.in; do
	echo "##### $f #####"
	base_name=${f%.*}

	# Remove old output file:
	rm "${base_name}.out"

	if [ -f "${base_name}.desc" ]; then
		cat "${base_name}.desc"
	fi
	./calc < $f > "${base_name}.out"
	if [ -f "${base_name}.res" ]; then
		diff_files=$(diff ${base_name}.res ${base_name}.out)
		if [ -z "${diff_files}" ]; then
			echo -e "\e[32mTest succesfull!\e[39m"
		else
			echo -e "\e[31m${diff_files}\e[39m"
		fi
	else
		echo -e "\e[33mNo correct response file(.res) found!\e[39m"
	fi
done

# test1: simple test to see if it can make a very basic simplification using an intermediate person
# test2: test if it is able to solve the closed independent loops betwen people((0,3), and (1,2))
# test3: 8 people, 
