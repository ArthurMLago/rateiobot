#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Simple Bot to reply to Telegram messages.

This program is dedicated to the public domain under the CC0 license.

This Bot uses the Updater class to handle the bot.

First, a few handler functions are defined. Then, those functions are passed to
the Dispatcher and registered at their respective places.
Then, the bot is started and runs until we press Ctrl-C on the command line.

Usage:
Basic Echobot example, repeats messages.
Press Ctrl-C on the command line or send a signal to the process to stop the
bot.
"""

import planilha
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
import logging
import time
import schedule
import datetime
import json
from threading import Event,Timer,Thread
from enum import Enum
#import calculadora.calculadora
import shelve
import traceback
import os
import queue
import subprocess


BOT_TOKEN = os.getenv("BOT_TOKEN", "Not set")

Users = {}

shouldQuit = Event()



pending_calculations = queue.Queue(5)
estado = {}
estado["matriz_transf"] = []
estado["ultimo_rateio_concluido"] = 24235 # era 2, deve ser 3
estado["estado_rateio"] = "comeco"
estado["pedidos_adiar"] = []
estado["pedidos_adiantar"] = []
estado["msg_id_rateio"] = []
waiting_to_make_calculations = None



#chat_id = -370825116
#chat_id = -1001241685802 # grupo dev
chat_id = -1001099492368 # grupo real

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)
# Define a few command handlers. These usually take the two arguments bot and
# update. Error handlers also receive the raised TelegramError object in error.
def start(bot, update):
    """Send a message when the command /start is issued."""
    update.message.reply_text('Hi!')
    update.message.reply_text(update.message.chat.id)


def help(bot, update):
    """Send a message when the command /help is issued."""
    update.message.reply_text("Esse é o bot de rateio da rep do francês furtivo.\n Todo dia ao meio dia ele verifica pendencias no rateio, então todo dia primeiro ele irá avisar que é hora de fazer o rateio, e pedir que todos terminem de preencher a planilha. Quando o rteio for feito, a mensagem será pinada para fácil acesso. Para indicar que você pagou sua parte do rateio, responda a mensagem no formato indicado(o formato estará na mensagem de rateio).\n Você pode pedir uma simulação do rateio desse mês enviando o comando /simulaRateio")

def tabelinha_rateio(mat):
    msg = "\n```\n"
    for i in range(0,8):
        for j in range(0,8):
            if (mat[i][j] != 0):
                msg += "%3s >% 7.2f > %3s\n"%(Users[i]['code'], mat[i][j], Users[j]['code'])
    msg += "```"
    if (msg == "\n```\n```"):
        msg = "\n``` nenhuma transação ```\n"
    print(msg)
    return msg

def echo(bot, update):
    global estado

    #print(estado["msg_id_rateio"])
    #print(update.message.reply_to_message.message_id)

    if (update.message.chat_id == chat_id and update.message.reply_to_message is not None and update.message.reply_to_message.message_id in estado["msg_id_rateio"]):
        print("resposta ao rateio")
        
        payFrom = None
        payTo = None
        pago = 0
        for i in range(0,8):
            if (update.message.from_user.id == Users[i]['id']):
                payFrom = i
        if (payFrom is None):
            print("who the hell are you?")
            update.message.reply_text("Mas quem diabos é você e porque você esta aqui?")
            return
        
        argumentos = update.message.text.split()
        print(update.message.text)
        if argumentos[0].lower() in ["pago", "paguei", "pagado", "/pago", "#pago"]:
            print("mensagem de pagamento")
            if (len(argumentos) > 1):
                # Procurar pela pessoa mencionada:
                for i in range(0,8):
                    for ent in update.message.entities:
                        if (ent.type == "mention"):
                            mention = argumentos[1][0:ent.length].replace('@','')
                            if (mention == Users[i]['username']):
                                print("Pagamento para o %s"%Users[i]['nickname'])
                                payTo = i
                    
                    if (argumentos[1].lower() == Users[i]['nickname']):
                        print("Pagamento para o %s"%Users[i]['nickname'])
                        payTo = i
                        
                    if (argumentos[1].lower() in Users[i]['aliases']):
                        print("Pagamento para o %s"%Users[i]['nickname'])
                        payTo = i
                if (payTo is None):
                    print("Nao achei quem pagar")
                    update.message.reply_text("Não entendi para quem você queria pagar")
                    return
                if (len(argumentos) > 2):
                    print(argumentos[2])
                    print(argumentos)
                    print("vai mandar o valor também")
                    valor = float(argumentos[2].replace(',', '.').replace('R$r ', ''))
                    print(valor)
                    pago += valor
                    estado["matriz_transf"][payFrom][payTo] -= pago

                    update.message.reply_text("%s pagou %.2f reais para %s" %(Users[payFrom]['nickname'], pago, Users[payTo]['nickname']))


                else:
                    print("pagou tudo para essa pessoa")
                    pago += estado["matriz_transf"][payFrom][payTo]
                    estado["matriz_transf"][payFrom][payTo] = 0
                    update.message.reply_text("%s pagou todo seu débito com %s, que eram %.2f reais" %(Users[payFrom]['nickname'], Users[payTo]['nickname'], pago))
            else:
                print("pagou tudo")
                
                for i in range(0,8):
                    pago += estado["matriz_transf"][payFrom][i]
                    estado["matriz_transf"][payFrom][i] = 0
                update.message.reply_text("%s quitou todos os seus débitos que somavam %.2f reais"%(Users[payFrom]['nickname'], pago))

                print(estado["matriz_transf"])
        finished = True
        for i in range(0,8):
            for j in range(0,8):
                if (estado["matriz_transf"][i][j] > 0.1):
                    finished = False
        if (finished == True):
            estado["ultimo_rateio_concluido"] += 1
            updater.bot.sendMessage(chat_id, "Rateio concluido com sucesso!\n Obrigado pessoal")
            estado["estado_rateio"] = "comeco"
        backup_to_file()



def error(bot, update, error):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, error)

def faz_rateio():
    global estado
    estado["estado_rateio"] = "esperando_pagamentos"
    mes = ((estado["ultimo_rateio_concluido"]) % 12)
    debts, media = planilha.get_debts(planilha.meses[mes%12])
    
    costs = None
    with open("custos.csv", "r") as f:
        costs = [[int(x) for x in f.readline().split()] for y in range(0,8)]
    print(costs)

    p_calc = subprocess.Popen("calculadora/calc", stdin=subprocess.PIPE, stdout=subprocess.PIPE)
    input = ""
    input += "8 "
    for i in range(0,8):
        for j in range(0,8):
            input += str(costs[i][j])
            input += " "
    for i in range(0,8):
        input += str(debts[i])
        input += " "

    out = p_calc.communicate(input.encode('utf-8'))
    sobras = [float(x) for x in out[0].decode('utf-8').split('\n')[0].split()]
    planilha.write_remaining(planilha.meses[((mes + 1)%12)], sobras)
    print(sobras)
    estado["matriz_transf"] = [[float(y) for y in out[0].decode('utf-8').split('\n')[1 + x].split()] for x in range(0,8)]
    #sent = updater.bot.sendMessage(chat_id, str(ret))
    msg = "#rateio\nPessoal, segue rateio para o mês de %s:\n"%(planilha.meses[mes]) + tabelinha_rateio(estado["matriz_transf"])
    msg += "Este rateio gastamos o equivalente a **R$%.2f** por pessoa\n" % float(media)

    tem_sobra = False
    for i in range(0,8):
        if abs(sobras[i]) >= 0.01:
            tem_sobra = True
    if (tem_sobra):
        msg += "Para facilitar os pagamentos, restaram receber:\n```\n"
        for i in range(0,8):
            if abs(sobras[i]) >= 0.01:
                msg += Users[i]["code"] + ": %.2f"%sobras[i] + "\n" 
        msg += "```\n"
        msg += "Essas sobras já foram escritas na planilha para o mês que vem\n"
    msg += "Para sinalizar que você pagou sua parte, responda essa mensagem uma uma mensagem nesse formato:\n"
    msg += "`pago [para_quem [quanto]]\n`"
    msg += "Vou me esforçar para entender"

    sent = updater.bot.sendMessage(chat_id, msg, parse_mode= 'Markdown')
    estado["msg_id_rateio"] = [sent.message_id]
    updater.bot.pin_chat_message(chat_id, estado["msg_id_rateio"][0])
    backup_to_file()

def dry_run(bot, update):
    now = datetime.datetime.now()
    mes = now.month + now.year*12 - 1
    msg = "⚠️Essa é uma simulação do rateio para o mês de %s.⚠️\n*⚠️Este não é o nosso rateio, apenas uma simulação!*⚠️"%(planilha.meses[mes%12])
    debts, media = planilha.get_debts(planilha.meses[mes%12])
    costs = None
    with open("custos.csv", "r") as f:
        costs = [[int(x) for x in f.readline().split()] for y in range(0,8)]
    print(debts)
    print(costs)

    p_calc = subprocess.Popen("calculadora/calc", stdin=subprocess.PIPE, stdout=subprocess.PIPE)
    input = ""
    input += "8 "
    for i in range(0,8):
        for j in range(0,8):
            input += str(costs[i][j])
            input += " "
    for i in range(0,8):
        input += str(debts[i])
        input += " "

    out = p_calc.communicate(input.encode('utf-8'))
    sobras = [float(x) for x in out[0].decode('utf-8').split('\n')[0].split()]
    simul = [[float(y) for y in out[0].decode('utf-8').split('\n')[1 + x].split()] for x in range(0,8)]
    #simul = calculadora.calculadora.solve(8, costs, debts)
    msg += tabelinha_rateio(simul)
    tem_sobra = False
    for i in range(0,8):
        if abs(sobras[i]) >= 0.01:
            tem_sobra = True
    if (tem_sobra):
        msg += "Para facilitar os pagamentos, restarão receber:\n```\n"
        for i in range(0,8):
            if abs(sobras[i]) >= 0.01:
                msg += Users[i]["code"] + ": %.2f"%sobras[i] + "\n" 
    msg += "Este rateio gastamos o equivalente a ** R$%.2f ** por pessoa\n" % float(media)
    print(msg)
    update.message.reply_text(msg, parse_mode="Markdown")
    
    


def checa_rateio():
    global estado
    global waiting_to_make_calculations
    now = datetime.datetime.now()
    print(now)
    mes = now.month + now.year*12
    print(mes)
    print(estado["ultimo_rateio_concluido"])
    if (mes > estado["ultimo_rateio_concluido"] + 1):
        if (estado["estado_rateio"] == "comeco"):
            updater.bot.sendMessage(chat_id, "Pessoal, está na hora de fechar o rateio, todos ja colocaram tudo na planilha?\nEm *8 horas* vou fazer as contas e enviar a lista de transferências.\n5 pessoas devem enviar /adiantar\_rateio para fazer as contas agora.\nSe desejarem adiar o rateio pra amanhã, 5 pessoas devem enviar /adiar\_rateio.\n", parse_mode= 'Markdown')
            estado["estado_rateio"] = "esperando_para_calcular"
            waiting_to_make_calculations = Timer(8*3600, faz_rateio)
            waiting_to_make_calculations.start()
            estado["pedidos_adiar"] = []
            estado["pedidos_adiantar"] = []
            print("enviou mensagem avisando que ta na hroa")
        elif (estado["estado_rateio"] == "esperando_pagamentos"):
            caloteiros = set()
            for i in range(0,8):
                for j in range(0,8):
                    if (estado["matriz_transf"][i][j] != 0):
                        caloteiros.add(Users[i]['id'])
            msg = "Pessoal, paguem o rateio.\nAinda falta:\n" + tabelinha_rateio(estado["matriz_transf"]) + "\n"
            for caloteiro in caloteiros:
                msg += "[corno](tg://user?id=" + str(caloteiro) + ") "
            sent = updater.bot.sendMessage(chat_id, msg, parse_mode= 'Markdown')
            estado["msg_id_rateio"] += [sent.message_id]
            
    print(waiting_to_make_calculations)
    backup_to_file()

        
def adiarRateio(bot, update):
    global estado
    global waiting_to_make_calculations
    if (estado["estado_rateio"] == "esperando_para_calcular"):
        if (update.message.from_user.id in estado["pedidos_adiar"]):
            print("%d ja havia pedido para adiar o rateio, ignorando"%update.message.from_user.id)
        else:
            print("Pedido para adiar o rateio de %d recebido"%update.message.from_user.id)
            estado["pedidos_adiar"].append(update.message.from_user.id)
            backup_to_file()
        if (len(estado["pedidos_adiar"]) >= 5):
            if (waiting_to_make_calculations != None):
                waiting_to_make_calculations.cancel()
            waiting_to_make_calculations = None
            estado["estado_rateio"] = "comeco"
            updater.bot.sendMessage(chat_id, "O rateio foi adiado, amanhã no mesmo horário recomeçarei o processo", parse_ode= 'Markdown')
            print("rateio adiado")
            backup_to_file()
    else:
        print("Nao estamos no modo de pedir para adiar")


def adiantarRateio(bot, update):
    global estado
    global waiting_to_make_calculations
    if (estado["estado_rateio"] == "esperando_para_calcular"):
        if (update.message.from_user.id in estado["pedidos_adiantar"]):
            print("%d ja havia pedido para adiantar o rateio, ignorando"%update.message.from_user.id)
        else:
            print("Pedido para adiantar o rateio de %d recebido"%update.message.from_user.id)
            estado["pedidos_adiantar"].append(update.message.from_user.id)
            backup_to_file()
        print(len(estado["pedidos_adiantar"]))
        if (len(estado["pedidos_adiantar"]) >= 5):
            if (waiting_to_make_calculations != None):
                waiting_to_make_calculations.cancel()
            waiting_to_make_calculations = None
            pending_calculations.put([faz_rateio], block=False)
            print(pending_calculations)
            #faz_rateio()
            print("rateio adiantado")
            backup_to_file()
    else:
        print("Nao estamos no modo de pedir para adiantar")

def simula_rateio(bot, update):
    pending_calculations.put((dry_run, bot, update))
    print(pending_calculations.qsize())


def sched_thread_monitor():
    while not shouldQuit.is_set():
        # do_my_thing()
        #print("bob")
        schedule.run_pending()
        shouldQuit.wait(60)

def calc_thread_monitor():
    while not shouldQuit.is_set():
        # do_my_thing()
        #print("bob")
        ja_simulou = 0
        dequed = 0
        try:
            dequed = pending_calculations.get(True, 20)
        except queue.Empty:
            dequed = None
        while(dequed != None):
            if (dequed[0] == faz_rateio):
                print("Desenfileirou um faz_rateio")
                faz_rateio()
            elif (dequed[0] == dry_run):
                print("desenfileirou uma simulacao")
                if (ja_simulou == 0):
                    ja_simulou = 1
                    dry_run(dequed[1], dequed[2])
                else:
                    print("tenttiva de spam")
            else:
                print("this should not happen")
            try:
                dequed = pending_calculations.get(True, 4)
            except queue.Empty:
                dequed = None




def backup_to_file():
    print("backipng up globals to variable")
    filename="estado.json"
    fp = open(filename,'w')
    fp.write(json.dumps(estado, indent=4))
    fp.close()



def main():
    global estado
    global Users
    global updater
    """Start the bot."""
    # Create the EventHandler and pass it your bot's token.
    updater = Updater(BOT_TOKEN)

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # on different commands - answer in Telegram
    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("adiar_rateio", adiarRateio))
    dp.add_handler(CommandHandler("adiantar_rateio", adiantarRateio))
    dp.add_handler(CommandHandler("simula_rateio", simula_rateio))
    dp.add_handler(CommandHandler("pago", echo))
    dp.add_handler(CommandHandler("help", help))

    # on noncommand i.e message - echo the message on Telegram
    dp.add_handler(MessageHandler(Filters.text, echo))

    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    with open("usuarios.json", "r") as f:
        Users = json.loads(f.read())
    print(Users)


    try:
        fp = open("estado.json", "r")
        estado = json.loads(fp.read())
        fp.close()
    except Exception:
        print("Could not open backup")
        traceback.print_exc()
    if estado["estado_rateio"] == "esperando_para_calcular":
        waiting_to_make_calculations = Timer(8*3600, faz_rateio)



    checa_rateio() 

    #checa_rateio()
    #_thread.start_new_thread( sched_thread_monitor, ())
    t1 = Thread(target=sched_thread_monitor, name="SchedThread")
    t1.start()
    t2 = Thread(target=calc_thread_monitor, name="CalcThread")
    t2.start()
    # return
    schedule.every().day.at("12:30").do(checa_rateio)

    # while(True):
        # try:
            # updater.bot.sendMessage(chat_id, randomword(10))
            # time.sleep(2)
        # except:
            # pass

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()
    shouldQuit.set()

if __name__ == '__main__':
    main()
